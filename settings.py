from dotenv import load_dotenv
from os import getenv

load_dotenv()

HOST = getenv("HOST")
USERNAME = getenv("USERNAME")
PASSWORD = getenv("PASSWORD")
INBOXPREFIX = getenv("INBOXPREFIX")

folder_suffixes = ['spam', 'ham', 'spam_reply']
